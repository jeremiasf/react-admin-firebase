// in src/relevamientos.js
import * as React from "react";
// tslint:disable-next-line:no-var-requires
import {
  Datagrid,
  List,
  Show,
  Create,
  Edit,
  Filter,
  DisabledInput,
  SimpleShowLayout,
  SimpleForm,
  TextField,
  TextInput,
  ShowButton,
  EditButton,
  DeleteButton,
  RichTextField,
  ReferenceField,
  SelectInput,
  ReferenceInput
} from "react-admin";
import RichTextInput from "ra-input-rich-text";

const RelevamientoFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Buscar" source="titulo" alwaysOn />
  </Filter>
);

export const RelevamientoList = (props) => (
  <List {...props} filters={<RelevamientoFilter />}>
    <Datagrid>
      <TextField source="titulo" />
      <RichTextField source="descripcion" />
      <ReferenceField label="User" source="user_id" reference="users">
        <TextField source="name" />
      </ReferenceField>
      <ShowButton label="" />
      <EditButton label="" />
      <DeleteButton label="" redirect={false} />
    </Datagrid>
  </List>
);

export const RelevamientoShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="createdate" />
      <TextField source="lastupdate" />
      <TextField source="titulo" />
      <RichTextField source="descripcion" />
      <ReferenceField label="User" source="user_id" reference="users">
        <TextField source="name" />
      </ReferenceField>
    </SimpleShowLayout>
  </Show>
);

export const RelevamientoCreate = (props) => (
  <Create {...props} >
    <SimpleForm>
      <TextInput source="titulo" />
      <RichTextInput source="descripcion" />
      <ReferenceInput
        label="User"
        source="user_id"
        reference="users"
      >
        <SelectInput label="Usuario Asignado" optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);

export const RelevamientoEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <DisabledInput source="createdate" />
      <DisabledInput source="lastupdate" />
      <TextInput source="titulo" />
      <RichTextInput source="descripcion" />
      <ReferenceInput
        label="Usuario asignado"
        source="user_id"
        reference="users"
      >
        <SelectInput label="User" optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);
