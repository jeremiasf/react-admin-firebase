export interface RAFirebaseOptions {
  rootRef?: string;
  app?: any;
  logging?: boolean;
  offline?: boolean;
  watch?: string[];
  dontwatch?: string[];
}
